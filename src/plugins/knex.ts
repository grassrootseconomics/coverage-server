import path from 'path';

import fp from 'fastify-plugin'
import { FastifyPluginAsync } from 'fastify'
import knex, { Knex } from 'knex'

declare module 'fastify' {
  interface FastifyInstance {
    knex: Knex
  }
}

const dbFolder = process.env.DB_FOLDER || path.join(process.cwd(), "db")
const config: Knex.Config = {
  client: 'better-sqlite3',
  connection: {
    filename: `${dbFolder}/${
      process.env.NODE_ENV === 'production' ? 'cs_prod.sqlite3' : 'cs_dev.sqlite3'
    }`,
  },
  useNullAsDefault: true,
}

const knexP: FastifyPluginAsync = fp(async (fastify) => {
  const knexC = knex(config)
  fastify.log.debug('Knex connected to db')
  fastify.decorate('knex', knexC)

  fastify.addHook('onClose', async (fastify) => {
    fastify.log.debug('Closing knex connection')
    await fastify.knex.destroy()
  })
})

export default knexP
