import phin from 'phin'

import log from '../util/log'

interface RequestConfig {
  headers: {
    'PRIVATE-TOKEN': string
  }
}

export default class Gitlab {
  protected requestConfig: RequestConfig

  /**
   * @param {string} PA-Token - Personal Access Token from Gitlab
   */
  constructor(protected privateAccessToken: string) {
    if (!privateAccessToken) throw new Error('Gitlab token is required')

    this.requestConfig = {
      headers: {
        'PRIVATE-TOKEN': privateAccessToken,
      },
    }
  }

  async getJobTrace(projectId: number, jobId: number) {
    try {
      const { body } = await phin({
        url: `https://gitlab.com/api/v4/projects/${projectId}/jobs/${jobId}/trace`,
        method: 'GET',
        parse: 'string',
        ...this.requestConfig,
      })

      return body
    } catch (error) {
      log.error(error)
    }
  }
}
