import { FastifyInstance } from 'fastify'
import app from './app'

async function start(fastify: FastifyInstance) {
  await fastify.ready()
  await fastify.listen(5000, '::')

  for (const signal of ['SIGINT', 'SIGTERM']) {
    process.once(signal, async () => {
      fastify.log.info(signal)
      await fastify.close()
      return process.exit(0)
    })
  }

  process.on('uncaughtExceptionMonitor', (error) => {
    fastify.log.fatal(error)
    process.exit(1)
  })
}

start(app)
