import { FastifyInstance } from 'fastify'

import { projects } from '../project'

export default async function (fastify: FastifyInstance) {
    fastify.get(
        `/`,
        async (req, res) => {
            const groupedBadges: Array<{
                project: string;
                badges: string[];
            }> = [];
            for (const project of projects) {
                let group = groupedBadges.find(i => i.project === project.projectName);
                if (!group) {
                    group = {
                        project: project.projectName,
                        badges: [],
                    };
                    groupedBadges.push(group);
                }
                for (const job of project.jobs) {
                    group.badges.push(job.badgeName);
                }
                group.badges = group.badges.sort();
            }
            return res.view('/templates/index.pug', { groupedBadges })
        },
    )
}
