import path from 'path'

import { FastifyInstance } from 'fastify'
import fastifyStatic from 'fastify-static';

export default async function (fastify: FastifyInstance) {
    fastify.register(fastifyStatic, {
        root: path.resolve(__dirname, '../../public'),
    })
}
