import { FromSchema } from 'json-schema-to-ts'
import { FastifyInstance, FastifyRequest } from 'fastify'

import { hasBadge } from '../project'
import schema from '../schema'
import selectBadge from '../util/badge'

interface Route {
  Params: FromSchema<typeof schema.report.badge>;
}

export default async function (fastify: FastifyInstance) {

  async function hasBadgePreHandler(req: FastifyRequest<Route>) {
    if (!hasBadge(req.params.badgeName)) {
      return fastify.httpErrors.notFound('UNKNOWN_PROJECT')
    }
  }

  fastify.get<Route>(
    `/:badgeName`,
    {
      schema: {
        params: schema.report.badge,
        response: {
          '2XX': schema.successfulResponse,
        },
      },
      preHandler: hasBadgePreHandler,
    },
    async (req) => {
      const { badgeName } = req.params

      const results = await fastify
        .knex('reports')
        .where('project', badgeName)
        .orderBy('created_at', 'desc')
        .limit(10)

      if (results.length < 1) {
        return fastify.httpErrors.notFound('No coverage reports found')
      }

      return {
        ok: true,
        message: `Results for ${badgeName}`,
        data: results,
      }
    },
  )

  fastify.get<Route>(
    `/badge/:badgeName`,
    {
      schema: {
        params: schema.report.badge,
      },
      preHandler: hasBadgePreHandler,
    },
    async (req, res) => {
      const { badgeName } = req.params

      const results = await fastify
        .knex('reports')
        .where('project', badgeName)
        .orderBy('created_at', 'desc')
        .limit(1)

      res.type('image/svg+xml')
      res.send(selectBadge(badgeName, results.length ? parseFloat(results[0].coverage) : 0.0))
    },
  )

  fastify.get<Route>(
    `/summary/:badgeName`,
    {
      schema: {
        params: schema.report.badge,
      },
      preHandler: hasBadgePreHandler,
    },
    async (req, res) => {
      const { badgeName } = req.params

      const results = await fastify
        .knex('reports')
        .where('project', badgeName)
        .orderBy('created_at', 'desc')
        .limit(10)

      if (results.length < 1) {
        return fastify.httpErrors.notFound('No coverage reports found')
      }

      return res.view('/templates/summary.pug', { pageTitle: badgeName, reports: results })
    },
  )
}
