import { FromSchema } from 'json-schema-to-ts'
import { FastifyInstance } from 'fastify'

import Gitlab from '../lib/gitlab'

import extractResult from '../util/coverageParser'
import { projects } from '../project'
import schema from '../schema'
import { SqliteError } from 'better-sqlite3'

const gitlab = new Gitlab(process.env.GITLAB_PA_TOKEN as string)

export default async function (fastify: FastifyInstance) {
  fastify.post<{
    Body: FromSchema<typeof schema.gitlab.webhook>
    Headers: FromSchema<typeof schema.gitlab.securityHeader>
  }>(
    `/webhook`,
    {
      schema: {
        headers: schema.gitlab.securityHeader,
        body: schema.gitlab.webhook,
        response: {
          '2XX': schema.successfulResponse,
        },
      },
      preHandler: async (req) => {
        if (!process.env.GITLAB_WEBHOOK_SECRET) {
          return
        }

        if (req.headers['x-gitlab-token'] !== (process.env.GITLAB_WEBHOOK_SECRET as string)) {
          return fastify.httpErrors.unauthorized()
        }
      },
    },
    async (req) => {
      const { build_id, build_name, build_status, sha, project_id, commit, build_finished_at } =
        req.body

      const project = projects.find(({ projectId }) => projectId === project_id)
      if (!project) {
        fastify.log.info(`UNKNOWN_PROJECT: rejected build ${build_id} from ${project_id}`)
        return {
          ok: true,
          message: 'UNKNOWN_PROJECT',
        }
      }

      const job = project.jobs.find(({ jobName }) => jobName === build_name)
      if (!job) {
        fastify.log.info(`UNKNOWN_SERVICE: rejected build ${build_id} from ${project_id}`)
        return {
          ok: true,
          message: 'UNKNOWN_SERVICE',
        }
      }

      if (!(build_status === 'success' || build_status === 'failed')) {
        fastify.log.info(`UNAVAILABLE_RESULTS: rejected build ${build_id} from ${project_id}`)
        return {
          ok: true,
          message: 'RESULTS_UNAVAILABLE',
        }
      }

      const rawTrace = await gitlab.getJobTrace(project_id, parseInt(build_id))
      const result = extractResult(job.lang, rawTrace as string)

      if (!result) {
        fastify.log.error(`PARSING_FAILED: rejected build ${build_id} from ${project_id}`)
        return fastify.httpErrors.badRequest(`Could not parse job ${build_id}`)
      }

      try {
        await fastify.knex('reports').insert({
          job_id: build_id,
          coverage: result,
          project: job.badgeName,
          sha: sha,
          commit_message: commit.message,
          created_at: build_finished_at,
        })

        fastify.log.info(
          `PARSED_SUCCESSFULLY: build ${build_id} for ${job.badgeName}: ${result}`,
        )

        return {
          ok: true,
          message: `Successfully obtained results for job ${build_id} at ${sha}`,
          data: { result },
        }
      } catch (error) {
        if (error instanceof SqliteError) {
          if (error.code === 'SQLITE_CONSTRAINT_UNIQUE') {
            return fastify.httpErrors.forbidden('Job id already exists')
          }
        }
        fastify.log.error(error)
        return fastify.httpErrors.internalServerError('Something went wrong')
      }
    },
  )
}
