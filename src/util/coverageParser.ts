const langRegex: { [key: string]: RegExp } = {
  // Total coverage
  py: /Total coverage:\s(.+)%/gi,
  // Total statement coverage
  js: /All files\s*\|\s*(\d{2}.\d{2})/gi,
}

export default function extractResult(lang: string, rawTrace: string) {
  const result = langRegex[lang].exec(rawTrace as string)
  langRegex[lang].lastIndex = 0

  if (result && result[1]) {
    return result[1]
  }

  return null
}
