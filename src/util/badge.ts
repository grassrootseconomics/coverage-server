const badges = {
  base: (projectName: string, coverage: number, color: string) => {
    const section1Length = 6.4 * (projectName.length + 8 + 3)
    const section1CoordX = (section1Length / 2) * 10
    const section1TextLength = (section1Length - 10) * 10

    const section2Length = 35
    const section2CoordX = (section1Length + (section2Length / 2)) * 10
    const section2TextLength = (section2Length - 10) * 10

    const totalLength = section1Length + section2Length

    return `
      <svg
        xmlns="http://www.w3.org/2000/svg"
        xmlns:xlink="http://www.w3.org/1999/xlink"
        width="${totalLength}"
        height="20"
        role="img"
        aria-label="coverage: ${coverage}%"
      >
        <title>coverage: ${coverage}%</title>
        <linearGradient id="s" x2="0" y2="100%">
          <stop offset="0" stop-color="#bbb" stop-opacity=".1"/>
          <stop offset="1" stop-opacity=".1"/>
        </linearGradient>
        <clipPath id="r">
          <rect width="${totalLength}" height="20" rx="3" fill="#fff"/>
        </clipPath>
        <g clip-path="url(#r)">
          <rect width="${section1Length}" height="20" fill="#555"/>
          <rect x="${section1Length}" width="${section2Length}" height="20" fill="${color}"/>
          <rect width="${totalLength}" height="20" fill="url(#s)"/>
        </g>
        <g fill="#fff" text-anchor="middle" font-family="Verdana,Geneva,DejaVu Sans,sans-serif" text-rendering="geometricPrecision" font-size="110">
          <text aria-hidden="true" x="${section1CoordX}" y="150" fill="#010101" fill-opacity=".3" transform="scale(.1)" textLength="${section1TextLength}">coverage</text>
          <text x="${section1CoordX}" y="140" transform="scale(.1)" fill="#fff" textLength="${section1TextLength}">${projectName} | coverage</text>
          <text aria-hidden="true" x="${section2CoordX}" y="150" fill="#010101" fill-opacity=".3" transform="scale(.1)" textLength="${section2TextLength}">${coverage}%</text>
          <text x="${section2CoordX}" y="140" transform="scale(.1)" fill="#fff" textLength="${section2TextLength}">${coverage}%</text>
        </g>
      </svg>`
  },
  highCov: (projectName: string, coverage: number) => {
    return badges.base(projectName, coverage, '#4c1')
  },
  midCov: (projectName: string, coverage: number) => {
    return badges.base(projectName, coverage, '#fe7d37')
  },
  lowCov: (projectName: string, coverage: number) => {
    return badges.base(projectName, coverage, '#e05d44')
  },
}

export default function selectBadge(projectName: string, coverage: number) {
  coverage = Math.round(coverage)
  if (coverage >= 85) {
    return badges.highCov(projectName, coverage)
  } else if (coverage >= 50) {
    return badges.midCov(projectName, coverage)
  } else {
    return badges.lowCov(projectName, coverage)
  }
}
