import { FastifyInstance } from 'fastify'

import gitlabHandlers from './handlers/gitlab'
import homeHandlers from './handlers/home'
import publicHandlers from './handlers/public'
import reportHandlers from './handlers/report'

export default async function router(fastify: FastifyInstance) {
  fastify.register(gitlabHandlers, { prefix: '/gitlab' })
  fastify.register(publicHandlers, { prefix: '/public' })
  fastify.register(reportHandlers, { prefix: '/report' })
  fastify.register(homeHandlers)
}
