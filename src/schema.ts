const schema = {
  report: {
    badge: {
      type: 'object',
      properties: {
        badgeName: { type: 'string' },
      },
      required: ['badgeName'],
    },
  },
  gitlab: {
    checkJob: {
      type: 'object',
      properties: {
        id: { type: 'number' },
        project: { type: 'number' },
        lang: { enum: ['js', 'py'] },
      },
      required: ['project', 'id', 'lang'],
    },
    webhook: {
      type: 'object',
      properties: {
        build_id: { type: 'string' },
        project_id: { type: 'number' },
        build_name: { type: 'string' },
        build_status: { type: 'string' },
        build_finished_at: { type: 'string' },
        commit: {
          type: 'object',
          properties: {
            message: { type: 'string' },
          },
        },
        sha: { type: 'string' },
      },
      required: [
        'build_id',
        'build_finished_at',
        'commit',
        'project_id',
        'build_name',
        'build_status',
        'sha',
      ],
    },
    securityHeader: {
      type: 'object',
      properties: {
        'x-gitlab-token': { type: 'string' },
      },
    },
  },
  successfulResponse: {
    type: 'object',
    properties: {
      ok: { type: 'boolean' },
      message: { type: 'string' },
      data: { type: 'object' },
    },
    required: ['ok', 'message'],
  },
} as const

export default schema
