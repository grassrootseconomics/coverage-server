interface Job {
  badgeName: string;
  jobName: string;
  lang: 'js' | 'py';
}

interface Project {
  projectId: number;
  projectName: string;
  jobs: Job[];
}

export function getProjectAndJob(badgeName: string): [Project | undefined, Job | undefined] {
  for (const project of projects) {
    for (const job of project.jobs) {
      if (job.badgeName === badgeName) {
        return [project, job];
      }
    }
  }
  return [undefined, undefined];
}

export function hasBadge(badgeName: string) {
  return !!getProjectAndJob(badgeName)[0];
}

export const projects: Project[] = [
  {
    projectId: 23727820,
    projectName: 'stack',
    jobs: [
      {
        badgeName: 'cic-cache',
        jobName: 'build-test-cic-cache',
        lang: 'py',
      },
      {
        badgeName: 'cic-eth',
        jobName: 'build-test-cic-eth',
        lang: 'py',
      },
      {
        badgeName: 'cic-meta',
        jobName: 'build-test-cic-meta',
        lang: 'js',
      },
      {
        badgeName: 'cic-notify',
        jobName: 'build-test-cic-notify',
        lang: 'py',
      },
      {
        badgeName: 'cic-ussd',
        jobName: 'build-test-cic-ussd',
        lang: 'py',
      },
    ],
  },

  {
    projectId: 26342005,
    projectName: 'contracts',
    jobs: [
      {
        badgeName: 'erc20-faucet',
        jobName: 'run-coverage',
        lang: 'py',
      },
    ],
  },

  {
    projectId: 30162490,
    projectName: 'contracts',
    jobs: [
      {
        badgeName: 'erc20-token-index',
        jobName: 'run-coverage',
        lang: 'py',
      },
    ],
  },

  {
    projectId: 26341770,
    projectName: 'contracts',
    jobs: [
      {
        badgeName: 'erc20-transfer-authorization',
        jobName: 'run-coverage',
        lang: 'py',
      },
    ],
  },

  {
    projectId: 26342103,
    projectName: 'contracts',
    jobs: [
      {
        badgeName: 'eth-accounts-index',
        jobName: 'run-coverage',
        lang: 'py',
      },
    ],
  },

  {
    projectId: 26341980,
    projectName: 'contracts',
    jobs: [
      {
        badgeName: 'eth-address-index',
        jobName: 'run-coverage',
        lang: 'py',
      },
    ],
  },

  {
    projectId: 26341764,
    projectName: 'contracts',
    jobs: [
      {
        badgeName: 'eth-contract-registry',
        jobName: 'run-coverage',
        lang: 'py',
      },
    ],
  },

  {
    projectId: 26341748,
    projectName: 'contracts',
    jobs: [
      {
        badgeName: 'eth-erc20',
        jobName: 'run-coverage',
        lang: 'py',
      },
    ],
  },

  {
    projectId: 26341752,
    projectName: 'contracts',
    jobs: [
      {
        badgeName: 'eth-owned',
        jobName: 'run-coverage',
        lang: 'py',
      },
    ],
  },
];
