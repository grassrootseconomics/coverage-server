import fastifySensible from 'fastify-sensible'
import fastify from 'fastify'
import pointOfView from 'point-of-view'
import pug from 'pug'

import log from './util/log'
import router from './router'
import knexPlugin from './plugins/knex'

const app = fastify({
  logger: log,
})

app
  .register(knexPlugin)
  .register(router)
  .register(fastifySensible)
  .register(pointOfView, {
    engine: {
      pug: pug,
    },
  })

export default app
