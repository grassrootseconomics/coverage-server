exports.up = (knex) => {
  return knex.schema.createTable('reports', (table) => {
    table.increments('id').primary()
    table.string('job_id').unique()
    table.string('coverage')
    table.string('project')
    table.string('sha')
    table.string('commit_message')
    table.datetime('created_at')
  })
}

exports.down = (knex) => {
  return knex.schema.dropTableIfExists('reports')
}
