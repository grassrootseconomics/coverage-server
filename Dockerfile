## Stage A: Installing dependencies.

FROM node:17-alpine as StageA

RUN apk add g++ make python3

COPY package*.json /tmp/npm/
RUN cd /tmp/npm && npm i

## Stage B: Preparing for runime execution.

FROM node:17-alpine

EXPOSE 5000
CMD ["node", "build/"]

ENV NODE_ENV=production

WORKDIR /app

COPY --from=stageA /tmp/npm/node_modules ./node_modules
COPY . .
RUN npm run build
