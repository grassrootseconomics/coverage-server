module.exports = {
  development: {
    client: 'better-sqlite3',
    connection: {
      filename: './db/cs_dev.sqlite3',
    },
    useNullAsDefault: true,
  },
  production: {
    client: 'better-sqlite3',
    connection: {
      filename: './db/cs_prod.sqlite3',
    },
    useNullAsDefault: true,
  },
}
