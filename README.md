## coverage-server

> Self hosted coverge reports server

### Why?

Mainstream coverage reporting services like Coveralls, CodeCov, CodeClimate restrict coverage reports to a single repo\*.

However the biggest issues with such services is vendor lock in and lack of support for some git servers like Gitea (Which we heavily rely on).

_\* Some providers allow for aggregation of reports or using sub projects with feature flags_

### About

This server is platform and coverage tool agnostic. As long as you have access to the coverage raw trace from the command line or CI tool, you can track coverage. In short:

`Get coverage run raw trace -> Extract average coverage value with a regex -> Save and display reports`

In the current version, We receive the raw trace from a Gitlab webhook.

**Endpoints**

- `POST /gitlab/webhook` - Protected endpoint to recieve coverage raw traces from jobs
- `GET /reports/summary/:project` - Display a coverage summary for the last 10 tests (HTML)
- `GET /reports/:project` - Return a JSON array for the last 10 tests
- `GET /reports/badge/:project` - Badges for your Markdown docs

### Setup

**Env Variables**

```bash
export GITLAB_PA_TOKEN=your_gitlab_pa_token
export GITALAB_WEBHOOK_SECRET=your_webhook_secret
export LOG_LEVEL=debug
export DB_FOLDER=$(pwd)/db
```

**Dev**

```bash
yarn db:create:dev
yarn seed
yarn dev
```

**Prod**

```bash
export NODE_ENV=production
yarn db:create:prod
yarn seed
yarn build
node build/
```
